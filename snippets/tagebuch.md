```
---
title: $CURRENT_YEAR-$CURRENT_MONTH-$CURRENT_DATE heute
id: $ZKN_ID
created: $CURRENT_YEAR-$CURRENT_MONTH-$CURRENT_DATE $CURRENT_HOUR:$CURRENT_MINUTE
wochentag: ${1:Wochentag einfügen}
---

# ${2:Uhrzeit einfügen} ${3:Betreff einfügen}
#${4:Schlagwort einfügen}

$0
```