```
---
title: $FILENAME$1
id: $ZKN_ID
created: $CURRENT_YEAR-$CURRENT_MONTH-$CURRENT_DATE $CURRENT_HOUR:$CURRENT_MINUTE
tags:
  - ${2:Schlagwort 1}
  - ${3:Schlagwort 2}
---

$0
```